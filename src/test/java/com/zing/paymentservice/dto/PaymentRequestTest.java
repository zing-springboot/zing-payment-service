package com.zing.paymentservice.dto;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class PaymentRequestTest {

    @Test
    public void testConstructorAndGetters() {
        // Create a PaymentRequest object
        PaymentRequest request = new PaymentRequest(123L, "Sample transaction", "Groceries", 100.0, "Credit");

        // Verify constructor and getters
        assertEquals(123L, request.getCustomerId());
        assertEquals("Sample transaction", request.getTransactionDescription());
        assertEquals("Groceries", request.getCategory());
        assertEquals(100.0, request.getAmount());
        assertEquals("Credit", request.getPaymentType());
    }

    @Test
    public void testToString() {
        // Create a PaymentRequest object
        PaymentRequest request = new PaymentRequest(456L, "Another transaction", "Utilities", 50.0, "Debit");

        // Verify toString method
        String expectedToString = "PaymentRequest(customerId=456, transactionDescription=Another transaction, category=Utilities, amount=50.0, paymentType=Debit)";
        assertEquals(expectedToString, request.toString());
    }

    @Test
    public void testEqualsAndHashCode() {
        // Create two equal PaymentRequest objects
        PaymentRequest request1 = new PaymentRequest(123L, "Sample transaction", "Groceries", 100.0, "Credit");
        PaymentRequest request2 = new PaymentRequest(123L, "Sample transaction", "Groceries", 100.0, "Credit");

        // Verify equals and hashCode methods
        assertEquals(request1, request2);
        assertEquals(request1.hashCode(), request2.hashCode());
    }

    // Add more test cases as needed...
}
