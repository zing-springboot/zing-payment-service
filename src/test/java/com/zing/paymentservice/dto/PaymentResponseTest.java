package com.zing.paymentservice.dto;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class PaymentResponseTest {

    @Test
    public void testConstructorAndGetters() {
        // Create a PaymentResponse object
        PaymentResponse response = new PaymentResponse("Success", "200");

        // Verify constructor and getters
        assertEquals("Success", response.getMessage());
        assertEquals("200", response.getStatus());
    }

    @Test
    public void testToString() {
        // Create a PaymentResponse object
        PaymentResponse response = new PaymentResponse("Error", "404");

        // Verify toString method
        String expectedToString = "PaymentResponse(message=Error, status=404)";
        assertEquals(expectedToString, response.toString());
    }

    // Add more test cases as needed...

    // You can also test equals, hashCode, and any custom methods you add to the class.
}
