package com.zing.paymentservice.exception;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ErrorResponseTest {

    @Test
    public void testConstructorAndGetters() {
        ErrorResponse errorResponse = new ErrorResponse("Error message", "500");

        assertEquals("Error message", errorResponse.getErrorMessage());
        assertEquals("500", errorResponse.getErrorCode());
    }

    // Add more test cases as needed...
}
