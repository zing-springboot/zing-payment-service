package com.zing.paymentservice.exception;

import org.junit.jupiter.api.Test;
import java.util.Collections;
import static org.junit.jupiter.api.Assertions.*;

public class APIErrorTest {

    @Test
    public void testConstructorAndGetters() {
        ErrorResponse errorResponse = new ErrorResponse("Error message", "500");
        APIError apiError = new APIError(Collections.singletonList(errorResponse));

        assertEquals(Collections.singletonList(errorResponse), apiError.getErrorResponses());
    }

    // Add more test cases as needed...
}
