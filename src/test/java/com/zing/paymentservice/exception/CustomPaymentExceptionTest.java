package com.zing.paymentservice.exception;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CustomPaymentExceptionTest {

    @Test
    public void testConstructorAndGetters() {
        // Create a CustomPaymentException object
        CustomPaymentException exception = new CustomPaymentException("Error message", "500");

        assertEquals("Error message", exception.getMessage());
        assertEquals("500", exception.getErrorCode());
    }

    @Test
    public void testToString() {
        // Create a CustomPaymentException object
        CustomPaymentException exception = new CustomPaymentException("Another error", "404");

        // Verify toString method
        String expectedToString = "CustomPaymentException{errorCode='404'}: Another error";
        assertEquals(expectedToString, exception.toString());
    }

    // Add more test cases as needed...

    // You can also test equals, hashCode, and any custom methods you add to the class.
}
