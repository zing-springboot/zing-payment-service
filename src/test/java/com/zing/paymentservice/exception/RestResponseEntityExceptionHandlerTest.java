package com.zing.paymentservice.exception;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

class RestResponseEntityExceptionHandlerTest {

    @Test
    public void testHandleProductServiceException() {
        CustomPaymentException exception = new CustomPaymentException("Error message", "404");
        RestResponseEntityExceptionHandler handler = new RestResponseEntityExceptionHandler();

        ResponseEntity<ErrorResponse> response = handler.handleProductServiceException(exception);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals("Error message", response.getBody().getErrorMessage());
        assertEquals("404", response.getBody().getErrorCode());
    }

    // Add more test cases as needed...
}
