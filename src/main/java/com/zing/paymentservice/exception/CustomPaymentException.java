package com.zing.paymentservice.exception;

import lombok.Data;

@Data
public class CustomPaymentException extends RuntimeException{


    private final String errorCode;

    public CustomPaymentException(String message, String errorCode)
    {
        super(message);
        this.errorCode = errorCode;
    }

}
