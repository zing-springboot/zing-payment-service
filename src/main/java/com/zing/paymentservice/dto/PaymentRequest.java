package com.zing.paymentservice.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class PaymentRequest {
    private long customerId;

    private String transactionDescription;

    private String category;

    private double amount;

    private String paymentType;

}
