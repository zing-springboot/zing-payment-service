package com.zing.paymentservice.controller;

import com.zing.paymentservice.dto.PaymentRequest;
import com.zing.paymentservice.dto.PaymentResponse;
import com.zing.paymentservice.service.PaymentService;
import jakarta.validation.Valid;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
    @RequestMapping("/payments/api/v1/")
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @PostMapping("/makePayment")
    public ResponseEntity<PaymentResponse> doPayment(@Valid @RequestBody  PaymentRequest paymentRequest) {
        paymentService.doPayment(paymentRequest);

        return ResponseEntity.status(HttpStatus.CREATED).body(new PaymentResponse("Successfully Created","SUCCESS"));
    }
}
