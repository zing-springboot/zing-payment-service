package com.zing.paymentservice.service;

import com.zing.paymentservice.dto.PaymentRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentServiceImpl implements PaymentService{

    @Autowired
    private PaymentKafkaProducerService paymentKafkaProducerService;


    @Override
    public void doPayment(PaymentRequest paymentRequest) {
        paymentKafkaProducerService.sendPaymentDetailsToTranscationTopic(paymentRequest);
    }
}
