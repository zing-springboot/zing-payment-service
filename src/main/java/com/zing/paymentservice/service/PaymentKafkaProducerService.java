package com.zing.paymentservice.service;

import com.zing.paymentservice.dto.PaymentRequest;

public interface PaymentKafkaProducerService{
    public void sendPaymentDetailsToTranscationTopic(PaymentRequest paymentRequest);
}
