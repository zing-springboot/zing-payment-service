package com.zing.paymentservice.service;

import com.zing.paymentservice.dto.PaymentRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import static com.zing.paymentservice.utils.PaymentConstants.TRANSACTION_KAFKA_TOPIC;

@Service
public class PaymentKafkaProducerServiceImpl implements PaymentKafkaProducerService{

    @Autowired
    private KafkaTemplate<String,PaymentRequest>  kafkaTemplate;

    @Override
    public void sendPaymentDetailsToTranscationTopic(PaymentRequest paymentRequest) {
        kafkaTemplate.send(TRANSACTION_KAFKA_TOPIC,paymentRequest);
    }
}
