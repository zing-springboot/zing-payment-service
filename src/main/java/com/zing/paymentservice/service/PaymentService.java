package com.zing.paymentservice.service;

import com.zing.paymentservice.dto.PaymentRequest;

public interface PaymentService {
    public void doPayment(PaymentRequest paymentRequest);
}
